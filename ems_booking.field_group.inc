<?php
/**
 * @file
 * ems_booking.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ems_booking_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_ems_info|node|ems_booking|form';
  $field_group->group_name = 'group_ems_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ems_booking';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_vertical_tabs';
  $field_group->data = array(
    'label' => 'EMS Info',
    'weight' => '14',
    'children' => array(
      0 => 'field_ems_booking_id',
      1 => 'field_ems_booking_type',
      2 => 'field_ems_group_type',
      3 => 'field_ems_status',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-ems-info field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_ems_info|node|ems_booking|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_general_info|node|ems_booking|form';
  $field_group->group_name = 'group_general_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ems_booking';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_vertical_tabs';
  $field_group->data = array(
    'label' => 'Main Content',
    'weight' => '13',
    'children' => array(
      0 => 'body',
      1 => 'field_ems_booking_date',
      2 => 'field_ems_calendar_logic',
      3 => 'title',
      4 => 'path',
      5 => 'redirect',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-general-info field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_general_info|node|ems_booking|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_vertical_tabs|node|ems_booking|form';
  $field_group->group_name = 'group_vertical_tabs';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ems_booking';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Vertical Tabs',
    'weight' => '0',
    'children' => array(
      0 => 'group_general_info',
      1 => 'group_ems_info',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-vertical-tabs field-group-tabs',
        'id' => '',
      ),
    ),
  );
  $field_groups['group_vertical_tabs|node|ems_booking|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('EMS Info');
  t('Main Content');
  t('Vertical Tabs');

  return $field_groups;
}
