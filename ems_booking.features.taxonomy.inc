<?php
/**
 * @file
 * ems_booking.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ems_booking_taxonomy_default_vocabularies() {
  return array(
    'ems_booking_types' => array(
      'name' => 'EMS Event Type',
      'machine_name' => 'ems_booking_types',
      'description' => 'Taxonomy for booking group types, imported from EMS',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'ems_calendar_logic' => array(
      'name' => 'EMS Calendar Logic',
      'machine_name' => 'ems_calendar_logic',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'ems_group_types' => array(
      'name' => 'EMS Group Type',
      'machine_name' => 'ems_group_types',
      'description' => 'Taxonomy for booking group types, imported from EMS',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'ems_statuses' => array(
      'name' => 'EMS Status',
      'machine_name' => 'ems_statuses',
      'description' => 'Taxonomy for booking statuses, imported from EMS',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
